var request = require('request');

module.exports = function (context, myQueueItem) {
    context.log("============================================Started");
    if (myQueueItem) {

        context.log(JSON.stringify(myQueueItem));

        request({
            method: 'POST',
            url: 'https://' + myQueueItem.tenant.hostName + '/one/rt/track/' +
                myQueueItem.tenant.siteKey + '/events',
            headers: myQueueItem.oneHeader,
            body: JSON.stringify(myQueueItem.body)
        }, function (error, response, body) {
            if (error) {
                context.log("-----] Error: " + error);
                var errMsg = {
                    "body": myQueueItem.body,
                    "url": 'https://' + myQueueItem.tenant.hostName + '/one/rt/track/' +
                        myQueueItem.tenant.siteKey + '/events',
                    "headers": myQueueItem.oneHeader
                }

                context.bindings.outputQueueItem = myQueueItem.body;
                context.done();
            } else {
                var oneMessage = JSON.parse(body);

                if (oneMessage.statusCode != 200) {

                    context.log("Status message not 200 ", oneMessage);
                    var errMsg = {
                        "oneMessage": oneMessage,
                        "body": myQueueItem.body,
                        "url": 'https://' + myQueueItem.tenant.hostName + '/one/rt/track/' +
                            myQueueItem.tenant.siteKey + '/events',
                        "headers": myQueueItem.oneHeader
                    }
                    context.bindings.outputQueueItem = errMsg;
                    context.done();
                } else {
                    context.log("Done ", oneMessage);
                    context.done();
                }
            }
        });
        context.done();
    } else {
        context.log("Error with queue message");
        context.done();
    }
};