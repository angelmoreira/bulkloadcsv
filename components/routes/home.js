var express = require('express');
var router = express.Router();

//extract later
var fs = require('fs');
var csv = require('fast-csv');
var multer = require('multer');
var storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, './')
    },
    filename: function (req, file, cb) {
        cb(null, file.fieldname + '-' + Date.now())
    }
});
var upload = multer({storage: storage});

var FileFunctions = require('../FileFunctions');

module.exports = router;


router.get('/', function(req, res){

    res.render("home.hbs"); //, {layout: 'layouts/app.hbs'});

});

router.post('/upload', upload.single('uploads'), function(req,res){
    var filename = req.file.filename;
    var fileFunctions = new FileFunctions();

    var stream = fs.createReadStream(filename);
 
    csv
    .fromStream(stream, {headers : true})
    .on("data", function(csvData){
        fileFunctions.readCsv(csvData, function(propertiesArr){
            fileFunctions.buildBodyAndSendToQ(req.body, propertiesArr);
        });
    })
    .on("end", function(){
        console.log("Done reading CSV. Deleting temporary file...");
        fs.unlink(filename, function (err) {
            if (err) {
                console.log("Error deleting temporary file: ", err);
            }
            else{
                res.send({"message":"Done, CSV sent to Queue", "status":200});
            }
        });
    });
});

