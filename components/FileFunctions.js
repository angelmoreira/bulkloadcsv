var request = require('request');
var AzureFunctions = require('./AzureFunctions');
var Azure = new AzureFunctions();

function FileFunctions() {}

FileFunctions.prototype.readCsv = function(data, callback){
    
    var keyArr = Object.keys(data)[0].split(",");
    var valuesArr = Object.values(data)[0].split(",");
    
    var propertyArr = [];
    var propertyObj = {}

    keyArr.forEach(function(key, ind){
        if(key.toLocaleLowerCase() === "callduration"){
            valuesArr[ind] = (valuesArr[ind].split(":")[0] * 60) + parseInt(valuesArr[ind].split(":")[1])
        }
    
        propertyObj = {};
        propertyObj = {
           "name" : key.toLocaleLowerCase(),
           "value" : valuesArr[ind]
        };
        propertyArr.push(propertyObj);

    });
    
    callback(propertyArr);

}

////////////////////////Extract to Azure fx app?
FileFunctions.prototype.buildBodyAndSendToQ = function(tenancy, csvProperties ){

    if(tenancy.siteKey){
        //var timestamp = Math.floor(stepMessage.activityContext.timestamp);
        var oneHeader = {
            'Accept': 'application/json',
            'Cache-Control': 'no-cache',
            'Content-Type': 'application/json'
            //,'X-ONE-Timestamp': timestamp
            }
        
        var oneRequestBody= {};

        //BUILDING body for ONE
        // oneRequestBody.customerKeyName = body.apiName;
        // oneRequestBody.customerKey = csvProperties.customer_tn;

        oneRequestBody.uri = tenancy.touchpoint;

        if(csvProperties){
            oneRequestBody.properties = csvProperties;
        }
        
        var msgToQ = {
            "tenant": tenancy,
            "body": oneRequestBody,
            "oneHeader": oneHeader
        }
        Azure.sendMessageToQueue(msgToQ);
        
    }
    else{
        console.log("No site key");
    }
}



module.exports = FileFunctions;