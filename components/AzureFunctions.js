// initialize the AZ Storage NPM
var azureStorage = require('azure-storage');
var retryOperations = new azureStorage.ExponentialRetryPolicyFilter();

const QueueMessageEncoder = azureStorage.QueueMessageEncoder;
//Connection string for **Presales loading** Azure account
var azure_connect_string = (process.env.AZURE_STORAGE_CONNECTION_STRING || "DefaultEndpointsProtocol=https;AccountName=amoreira;AccountKey=7s/eEpTZFxplBgtw8wmv3f4v0HnZu+ESlj3Y/P+2y4fK5MV7K1MJR/EdIWwY45/cmBGM8FO+6w+c0buUgBFoOQ==;BlobEndpoint=https://amoreira.blob.core.windows.net/;QueueEndpoint=https://amoreira.queue.core.windows.net/;TableEndpoint=https://amoreira.table.core.windows.net/;FileEndpoint=https://amoreira.file.core.windows.net/;" );

var queueSvc = azureStorage.createQueueService(azure_connect_string).withFilter(retryOperations);
queueSvc.messageEncoder = new QueueMessageEncoder.TextBase64QueueMessageEncoder();
//Queue name to be written to
var queue_name = process.env.AZURE_QUEUE_NAME || "bulkload";

function AzureFunctions(){
    this.createQueue();
}

//Create queue if it doesnt exists. Azure account is set above as an env variable
AzureFunctions.prototype.createQueue = function(callback){
    queueSvc.createQueueIfNotExists(queue_name, function(error){
        if(!error){
            console.log("Queue created")
        }
    });
};

AzureFunctions.prototype.sendMessageToQueue = function (messageToSend) {
    // var util = require('util');
    // console.log("=========================SENDING=============================================\n"
    //  + util.inspect(messageToSend, false, null))
    
    queueSvc.createMessage(queue_name, JSON.stringify(messageToSend), function (error) {
        if(error){
            console.log("ERROR sending message to queue: ", error);
        }
    });
};

module.exports = AzureFunctions;

