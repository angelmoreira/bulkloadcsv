var express = require('express');
var app = express();

var bodyParser = require('body-parser');

var hbs = require('hbs');

var server = app.listen(process.env.PORT || 8080, function(){
    console.log("App up on: %s.", server.address().port);
});

app.set('view engine', 'hbs');

hbs.registerHelper("inc", function(value, options)
{
    return parseInt(value) + 1;
});

app.use("/", express.static('public'));
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.use(require('./components/routes'));